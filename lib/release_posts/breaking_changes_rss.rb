# frozen_string_literal: true

require 'gitlab'
require 'nokogiri'
require_relative '../api_retry'

module ReleasePosts
  class BreakingChangesRSS
    include ApiRetry
    include Helpers

    BREAKING_CHANGES_FILE = File.expand_path('../../sites/uncategorized/source/breaking-changes.xml', __dir__)

    def initialize
      connect
      merge_requests
    end

    def create
      setup
      erase
      build
      save
    end

    def setup
      log_info("Read the BREAKING_CHANGES_FILE from #{BREAKING_CHANGES_FILE}...")
      xml_string = File.read(BREAKING_CHANGES_FILE)

      log_info("Create the Nokogiri document object...")
      @doc = Nokogiri::XML(xml_string)

      @channel = @doc.at_css("channel")
    end

    def erase
      log_info("Remove the previous items before rewriting new ones...")

      @doc.search("item").remove
    end

    def build
      log_info("Build the new XML file with the fetched MRs...")

      @all_mr_objects.each do |change|
        log_info("Creating the item for #{change.title}...")
        item = Nokogiri::XML::Node.new("item", @doc)

        # create a title element and set its content to the change title
        title = Nokogiri::XML::Node.new("title", @doc)
        title.content = change.title
        item.add_child(title)

        # create a guid element and set its content to the change guid
        guid = Nokogiri::XML::Node.new("guid", @doc)
        guid.content = change.link
        item.add_child(guid)

        # create a link element and set its content to the change link
        link = Nokogiri::XML::Node.new("link", @doc)
        link.content = change.link
        item.add_child(link)

        # create a description element and set its description to the change description
        description = Nokogiri::XML::Node.new("description", @doc)

        # replace markdown hyperlinks with an <a> tag
        # https://rubular.com/r/7gTF1FiQwa1Bso
        description.inner_html = change.description.gsub /\[([^\]]+)\]\(([^)]+)\)/, '<a href="\2">\1</a>'
        description.inner_html = description.inner_html.delete("\n")
        description.inner_html = "<![CDATA[#{description.inner_html}]]>"
        item.add_child(description)

        # create a pubDate element and set its content to the change pubDate
        pub_date = Nokogiri::XML::Node.new("pubDate", @doc)
        pub_date.content = change.date
        item.add_child(pub_date)

        log_info("Adding #{change.title} to the document...")
        @channel.add_child(item)
      end
    end

    def save
      log_info("Saving to #{BREAKING_CHANGES_FILE}...")
      File.open(BREAKING_CHANGES_FILE, "w") { |file| file.write(@doc) }
    end

    private

    def connect
      log_info("Connecting to GitLab...")

      Gitlab.configure do |config|
        config.endpoint = 'https://gitlab.com/api/v4'

        config.private_token = ENV.fetch('GITLAB_BOT_TOKEN', nil) || ENV.fetch('PRIVATE_TOKEN', nil)
      end
    end

    def merge_requests
      log_info("Retrieving MRs from API...")
      api_retry do
        breaking_change_mrs_from_api =
          Gitlab
            .merge_requests(GITLAB_PROJECT_ID, state: 'merged', order_by: 'updated_at', labels: ['release post item::deprecation', 'breaking change'])
            .paginate_with_limit(200)

        log_info("Creating MR objects...")
        @all_mr_objects = breaking_change_mrs_from_api.map do |mr|
          MergeRequest.new(mr.iid, mr.project_id, mr.labels)
        rescue StandardError
          log_error("Error creating MergeRequest object for MR IID: #{mr.iid}")
          # Return nil to remove the element from the array
          nil
        end
        log_info("Removing nil values from the array")
        @all_mr_objects.compact!
      end
    end
  end
end
