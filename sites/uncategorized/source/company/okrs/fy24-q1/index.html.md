---
layout: markdown_page
title: "FY24-Q1 OKRs"
description: "View GitLabs Objective-Key Results for FY24 Q1. Learn more here!"
canonical_path: "/company/okrs/fy24-q1/"
---

This [fiscal quarter](/handbook/finance/#fiscal-year) will run from February 1, 2023 to April 30, 2023.

The source of truth for FY24-Q1 OKRs will be in GitLab.

## On this page
{:.no_toc}

- TOC
{:toc}

## OKR Schedule

In order to start dogfooding OKRs in GitLab, we are using a modified schedule for FY24-Q1 OKRs.   
We will resume the [by-the-book schedule](/company/okrs/#okr-process-at-gitlab) for next quarter's OKRs.

### Modified Schedule for FY24-Q1

| OKR schedule | Week of | Task |
| ------ | ------ | ------ |
| -5 | 2022-12-26 | CEO shares top goals with E-group for feedback |
| -4 | 2023-01-02 | CEO shares top goals in #okrs Slack channel |
| -4 | 2023-01-02 | E-group propose OKRs for their functions in the OKR draft review meeting agenda |
| -3 | 2023-01-09 | E-group 50 minute draft review meeting | 
| -2 | 2023-01-16 | E-group discusses with their respective teams and polishes OKRs |
| -2 | 2023-01-23 | CEO top goals available in GitLab | 
| -2 | 2023-01-23 | Function OKRs are put into GitLab and links are shared in #okrs Slack channel |
| -1 | 2023-01-23 | CEO reports post links to final OKRs in #okrs slack channel and @ mention the CEO and CoS to the CEO for approval |
| 0  | 2023-01-30 | CoS to the CEO updates OKR page for current quarter to be active and includes CEO level OKRs with consideration to what is public and non-public |


## OKRs

The source of truth for GitLab OKRs and KRs is [GitLab](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=created_date&state=opened&type%5B%5D=key_result&label_name%5B%5D=CEO%20OKR&first_page_size=20). CEO objectives and KRs are captured on this page. 

### 1. [CEO: Increase adoption through **customer results** and increasing operating income (NGOI)](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/121988141) 
1. **CEO KR**: Increase Ultimate adoption by X% and prevent churn by Y% versus current FY24-Q1 estimates: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/121994401
1. **CEO KR**: Remove all manual steps in Fulfillment to enable self-serve: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/121991887
1. **CEO KR**: Deliver Value Stream Dashboards MVC 1 Beta: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/121991904

### 2. CEO: [Mature the Platform to be the leading DevSecOps platform](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/121988427)
1. **CEO KR**: Deliver a clear comparison that is linked to the roadmap: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/121992976 
1. **CEO KR**: Complete Best in Class (BIC) delta milestones for FY24-Q1 for all groups: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/121993000
1. **CEO KR**: Figure out what we need to do to move the needle on SUS. Should include specific deliverables that are measurable: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/121993931
1. **CEO KR**: Achieve AI Assisted Code Suggestions MVC open beta (AI Assisted BIC): https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/121994196

### 3. CEO: [Grow careers through encouraging a customer result mindset](https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/121988698)
1. **CEO KR**: Get closer to our customers through conducting value stream assessments with 100% of global top 30 customers by Ultimate ARR: https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/121994290
1. **CEO KR**: Increase underrepresented group manager from 13% to >=15% (US): https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/123087603
1. **CEO KR**: Maintain GitLab's fast pace and focus on results through [bias for action](/handbook/values#bias-for-action), [iteration](/handbook/values#move-fast-by-shipping-the-minimal-viable-change), and [sense of urgency](/handbook/values#sense-of-urgency): https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/121994365
1. **CEO KR**: Co-create with our customers by activating 20 [leading orgs](/marketing/community-relations/leading-organizations): https://gitlab.com/gitlab-com/gitlab-OKRs/-/work_items/121994401 
